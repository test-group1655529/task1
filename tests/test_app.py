# tests/test_app.py
import unittest
from app import say_hello

class TestApp(unittest.TestCase):
    def test_say_hello(self):
        result = say_hello()
        self.assertEqual(result, "Hello, World!")

if __name__ == "__main__":
    unittest.main()
